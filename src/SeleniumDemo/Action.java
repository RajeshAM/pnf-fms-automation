package SeleniumDemo;



import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import functionLibraries.CommonLibrary;
import functionLibraries.WebDriverManager;


public class Action {
	
	public static int WAIT_EXPLICIT = Integer.parseInt(getConsoleInput("explicitWait"));
	/**
	 * Objective: To get the value from console property file
	 * @param key
	 * @return property value
	 */
	public static String getConsoleInput(String key)
	{
		return CommonLibrary.consoleProps.getProperty(key);
	}
	
	/**
	 * Objective: To wait for the element and perform click operation.
	 * 
	 * @param e
	 * @param waitBeforeActionBoolean
	 * @author rajesh
	 */
	public void click(WebElement e, Boolean waitBeforeActionBoolean)
	{
		if (waitBeforeActionBoolean)
		{
			waitForElementToBeVisible(e);
			waitForElementToBeClickable(e);
		}
		e.click();
	}

	/**
	 * Objective: To Click on any element without waiting for that element.
	 * @param e
	 * @author rajesh
	 */
	public void click(WebElement e)
	{
		click(e, false);
	}
	
	
	
	
	/**
	 * Objective : Sleep and catch the exception.
	 * 
	 * @param seconds
	 *            the number of seconds to wait
	 * @throws InterruptedException 
	 */
	public void wait(int seconds) throws InterruptedException
	{
		Thread.sleep(seconds * 1000);
	}

	/**
	 * Objective : To wait for page to load
	 * @throws MalformedURLException 
	 */
	public void waitForPageToLoad() throws MalformedURLException
	{
		//		logger.info("Waiting for page to load");
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>()
				{
			public Boolean apply(WebDriver driver)
			{
				return ((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete");
			}
				};

//				Wait<WebDriver> wait = new WebDriverWait(WebDriverManager.getThreadLocalDriver(), WAIT_PAGE_LOAD);
//				WebDriverWait wait = new WebDriverWait(driver,WAIT_PAGE_LOAD);
				WebDriverWait wait = new WebDriverWait(getDriver("ie", "local"),120);
				wait.until(expectation);
				//				logger.info("Page load completed.");		

	}

	/**
	 * Objective : To wait for given element (By) to be present
	 * @param locator
	 */
	public void waitForElementToBePresent(By locator)
	{
		new WebDriverWait(WebDriverManager.getThreadLocalDriver(), WAIT_EXPLICIT)
		.until(ExpectedConditions.presenceOfElementLocated(locator));
	}

	/**
	 * Objective : To wait for element to be invisible
	 * @param e
	 */
	public void waitForElementToBeVisible(WebElement e)
	{
		new WebDriverWait(WebDriverManager.getThreadLocalDriver(), WAIT_EXPLICIT)
		.until(ExpectedConditions.visibilityOf(e));
	}

	/**
	 * Objective : To wait for button to be clickable
	 * 
	 * @param element
	 */
	public void waitForElementToBeClickable(WebElement element)
	{
		new WebDriverWait(WebDriverManager.getThreadLocalDriver(), WAIT_EXPLICIT)
		.until(ExpectedConditions.elementToBeClickable(element));
	}

	/**
	 * Objective : To check whether element is displayed or not
	 * 
	 * @param e
	 * @return
	 */
	public boolean isElementDisplayed(WebElement e)
	{
		return e.isDisplayed();
	}
	
	/**
	 * Objective: To get the RemoteWebDriver or Local driver.
	 * @param browserName
	 * @param environmentType
	 * @return
	 * @throws MalformedURLException
	 * @author rajesh
	 */

	public WebDriver getDriver(String browserName, String environmentType)
			throws MalformedURLException
			{
		WebDriver driver = null;
		DesiredCapabilities dc = null;
		switch (browserName)
		{
		
		case "ie":
			//			logger.info("==========:::: Initialized IE driver ::::===========");
			DesiredCapabilities browserCapabillities = DesiredCapabilities
			.internetExplorer();
			browserCapabillities.setCapability(
					CapabilityType.ACCEPT_SSL_CERTS, true);
			browserCapabillities
			.setCapability(
					InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
					true);

			System.setProperty("webdriver.ie.driver",
					"lib\\IEDriverServer.exe");

			if (environmentType.equals("local"))
				driver = new InternetExplorerDriver(browserCapabillities);
			//			logger.info("==========:::: Initialized IE driver ::::===========");
		}
		if (environmentType.equals("remote"))
			driver = new RemoteWebDriver(new URL(
					getConsoleInput("seleniumHubURL")), dc);
		return driver;
			}


	
	/**
	 * Objective: To wait for the Web Element and enter value.
	 * 
	 * @param e
	 * @param strValue
	 * @param waitBeforeActionBoolean
	 * @author rajesh
	 */
	public void enterValue(WebElement e, CharSequence strValue,
			Boolean waitBeforeActionBoolean)
	{
		if (waitBeforeActionBoolean)
		{
			waitForElementToBeVisible(e);
		}
		e.sendKeys(strValue);
	}

	/**
	 * Objective: To enter value in any element without wait.
	 * @param e
	 * @param strValue
	 * @param waitBeforeActionBoolean
	 * @author rajesh
	 */
	public void enterValue(WebElement e, CharSequence strValue)
	{
		enterValue(e, strValue, false);
	}

	/**
	 * Objective: To Select value from DropDown by Visible Text without wait.
	 * 
	 * @param e
	 * @param strValue
	 * @param waitBeforeActionBoolean
	 * @author rajesh
	 */
	public void selectValueFromDropDown(WebElement e, String strValue)
	{
		selectValueFromDropDown(e, strValue, false);
	}

	/**
	 * Objective: To Select value from DropDown by Visible Text with wait.
	 * 
	 * @param e
	 * @param strValue
	 * @param waitBeforeActionBoolean
	 * @author rajesh
	 */
	public void selectValueFromDropDown(WebElement e, String strValue,
			Boolean waitBeforeActionBoolean)
	{
		if (waitBeforeActionBoolean)
		{
			waitForElementToBeVisible(e);
		}
		Select select = new Select(e);
		select.selectByVisibleText(strValue);
	}
	
	/**
	 * Objective : To wait for button to be clickable
	 * 
	 * @param element
	 */
	public void waitForElementToBeClickable1(WebElement element)
	{
		new WebDriverWait(WebDriverManager.getThreadLocalDriver(), WAIT_EXPLICIT)
		.until(ExpectedConditions.elementToBeClickable(element));
	}

	/**
	 * Objective : To check whether element is displayed or not
	 * 
	 * @param e
	 * @return
	 */
	public boolean isElementDisplayed1(WebElement e)
	{
		return e.isDisplayed();
	}

	/**
	 * Objective : To scroll down to given vertical point
	 * 
	 * @param verPoint
	 *            the vertical point
	 * @throws InterruptedException 
	 */
	public void moveScrollBar(int verPoint) throws InterruptedException
	{
		((JavascriptExecutor) WebDriverManager.getThreadLocalDriver())
		.executeScript("window.scroll(0, " + verPoint + ");");
		this.wait(1);
	}

	/**
	 * Objective : To scroll down to given Web Element
	 * 
	 * @param e
	 * @return
	 * @throws InterruptedException 
	 */
	public WebElement scrollToElement(WebElement e) throws InterruptedException
	{
		Coordinates coordinate = ((Locatable) e).getCoordinates();
		coordinate.onPage();
		coordinate.inViewPort();
		moveScrollBar(e.getLocation().y);
		return e;
	}

	/**
	 * Objective: To clear the element
	 * 
	 * @param e
	 */
	public void clear(WebElement e)
	{
		e.clear();
	}
	
}	
	
