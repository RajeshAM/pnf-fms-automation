package functionLibraries;



import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import functionLibraries.CommonLibrary;

public class Action
{
	//	private static final Logger logger = new Logger(Action.class);

	public WebDriver driver;
	public static int WAIT_PAGE_LOAD = Integer.parseInt(getConsoleInput("pageLoadWait"));
	public static int WAIT_IMPLICIT = Integer.parseInt(getConsoleInput("implicitWait"));
	public static int WAIT_EXPLICIT = Integer.parseInt(getConsoleInput("explicitWait"));
	public static int WAIT_AJAX = Integer.parseInt(CommonLibrary.consoleProps.getProperty("ajaxWait"));

	/**
	 * Objective: To get the value from console property file
	 * @param key
	 * @return property value
	 */
	public static String getConsoleInput(String key)
	{
		return CommonLibrary.consoleProps.getProperty(key);
	}

	/**
	 * Objective: To Select value from DropDown by index with wait.
	 * 
	 * @param e
	 * @param strValue
	 * @param waitBeforeActionBoolean
	 * @author rajesham
	 */
	public void selectByIndexFromDropDown(WebElement e, int index,
			Boolean waitBeforeActionBoolean)
	{
		if (waitBeforeActionBoolean)
		{
			System.out.println();
			waitForElementToBeVisible(e);
		}
		Select select = new Select(e);
		select.selectByIndex(index);
	}

	/**
	 * Objective: To wait for the element and perform click operation.
	 * 
	 * @param e
	 * @param waitBeforeActionBoolean
	 * @author rajesham
	 */
	public void click(WebElement e, Boolean waitBeforeActionBoolean)
	{
		if (waitBeforeActionBoolean)
		{
			waitForElementToBeVisible(e);
			waitForElementToBeClickable(e);
		}
		e.click();
	}

	/**
	 * Objective: To Click on any element without waiting for that element.
	 * @param e
	 * @author rajesham
	 */
	public void click(WebElement e)
	{
		click(e, false);
	}

	/**
	 * Objective: To wait for the Web Element and enter value.
	 * 
	 * @param e
	 * @param strValue
	 * @param waitBeforeActionBoolean
	 * @author rajesham
	 */
	public void enterValue(WebElement e, CharSequence strValue,
			Boolean waitBeforeActionBoolean)
	{
		if (waitBeforeActionBoolean)
		{
			waitForElementToBeVisible(e);
		}
		e.sendKeys(strValue);
	}

	/**
	 * Objective: To enter value in any element without wait.
	 * @param e
	 * @param strValue
	 * @param waitBeforeActionBoolean
	 * @author rajesham
	 */
	public void enterValue(WebElement e, CharSequence strValue)
	{
		enterValue(e, strValue, false);
	}

	/**
	 * Objective: To Select value from DropDown by Visible Text without wait.
	 * 
	 * @param e
	 * @param strValue
	 * @param waitBeforeActionBoolean
	 * @author rajesham
	 */
	public void selectValueFromDropDown(WebElement e, String strValue)
	{
		selectValueFromDropDown(e, strValue, false);
	}

	/**
	 * Objective: To Select value from DropDown by Visible Text with wait.
	 * 
	 * @param e
	 * @param strValue
	 * @param waitBeforeActionBoolean
	 * @author rajesham
	 */
	public void selectValueFromDropDown(WebElement e, String strValue,
			Boolean waitBeforeActionBoolean)
	{
		if (waitBeforeActionBoolean)
		{
			waitForElementToBeVisible(e);
		}
		Select select = new Select(e);
		select.selectByVisibleText(strValue);
	}

	/**
	 * Objective: To retrieve Text of an element without wait.
	 * @param e
	 * @param driver
	 * @throws Exception
	 * @author rajesham
	 */
	public String getText(WebElement e) throws Exception
	{
		return getText(e, false);
	}

	/**
	 * Objective: To retrieve Text of an element with wait
	 * 
	 * @param e
	 * @param driver
	 * @param waitBeforeActionBoolean
	 * @throws Exception
	 * @author rajesham
	 */
	public String getText(WebElement e, Boolean waitBeforeActionBoolean)
			throws Exception
			{
		if (waitBeforeActionBoolean)
		{
			waitForElementToBeVisible(e);
		}
		return ((JavascriptExecutor) WebDriverManager.getThreadLocalDriver()).executeScript(
				"return jQuery(arguments[0]).text();", e).toString().trim();
			}

	/**
	 * Objective: To Select Value From AutoComplete Text box with wait.
	 * 
	 * @param e
	 * @param driver
	 * @param strValue
	 * @param waitBeforeActionBoolean
	 * @throws Exception
	 * @author rajesham
	 *  
	 */

	public void selectFromAutoComplete(WebElement element, String keyWord) throws InterruptedException 
	{ 
		click(element, true);
		enterValue(element, keyWord.substring(0, keyWord.length()-1));      
		waitForAjaxToLoad();

		//By desiredRecord  = By.xpath("//*[starts-with(text(),'" + keyWord + "')]");
		By desiredRecord  = By.xpath("//a[contains(text(),'" + keyWord + "')]");
		waitForElementToBeVisible(desiredRecord);

		if(!WebDriverManager.getThreadLocalDriver().findElement(desiredRecord).isDisplayed())
		{
			clear(element);
			enterValue(element, keyWord.substring(0, keyWord.length()-1));      
			waitForAjaxToLoad();
			waitForElementToBePresent(desiredRecord);
		}
		WebDriverManager.getThreadLocalDriver().findElement(desiredRecord).click();
	}

	/**
	 * Objective: To get the RemoteWebDriver or Local driver.
	 * @param browserName
	 * @param environmentType
	 * @return
	 * @throws MalformedURLException
	 * @author rajesham
	 */
	@SuppressWarnings("deprecation")
	public WebDriver getDriver(String browserName, String environmentType)
			throws MalformedURLException
			{
		WebDriver driver = null;
		DesiredCapabilities dc = null;
		switch (browserName)
		{
		case "firefox":
			//			logger.info("---------------: Inside firefox profile :--------------");
			dc = DesiredCapabilities.firefox();
			dc.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
					UnexpectedAlertBehaviour.ACCEPT);
			dc.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
			dc.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			dc.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING,
					true);
			dc.setCapability(CapabilityType.HAS_NATIVE_EVENTS, true);
			if (environmentType.equals("local"))
				driver = new FirefoxDriver(dc);
			//			logger.info("==========:::: Initialized firefox driver ::::===========");
			break;
		case "chrome":
			//			logger.info("---------------: Inside chrome profile :--------------");
			DesiredCapabilities chro = DesiredCapabilities.chrome();
			chro.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
					UnexpectedAlertBehaviour.ACCEPT);
			chro.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			chro.setCapability(CapabilityType.HAS_NATIVE_EVENTS, true);
			chro.setCapability(CapabilityType.SUPPORTS_FINDING_BY_CSS, true);
			chro.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
			chro.setCapability(CapabilityType.SUPPORTS_ALERTS, true);
			chro.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING,
					true);
			System.setProperty("webdriver.chrome.driver",
					"lib\\chromeDriver.exe");

			if (environmentType.equals("local"))
				driver = new ChromeDriver();
			

			//			logger.info("==========:::: Initialized Chrome driver ::::===========");
			break;
		case "opera":
			//			logger.info("---------------: Inside opera profile :--------------");
			DesiredCapabilities opera = DesiredCapabilities.chrome();
			opera.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
					UnexpectedAlertBehaviour.IGNORE);
			opera.setCapability("opera.binary", "lib\\launcher.exe");

			//			if (environmentType.equals("local"))
			//				driver = new OperaDriver(opera);
			//			logger.info("==========:::: Initialized opera driver ::::===========");
			break;
		case "ie":
			//			logger.info("==========:::: Initialized IE driver ::::===========");
			DesiredCapabilities browserCapabillities = DesiredCapabilities
			.internetExplorer();
			browserCapabillities.setCapability(
					CapabilityType.ACCEPT_SSL_CERTS, true);
			browserCapabillities
			.setCapability(
					InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
					true);

			System.setProperty("webdriver.ie.driver",
					"lib\\IEDriverServer.exe");

			if (environmentType.equals("local"))
				driver = new InternetExplorerDriver(browserCapabillities);
			//			logger.info("==========:::: Initialized IE driver ::::===========");
		}
		if (environmentType.equals("remote"))
			driver = new RemoteWebDriver(new URL(
					getConsoleInput("seleniumHubURL")), dc);
		return driver;
			}

	//	/**
	//	 * To set the logger
	//	 * @param logger
	//	 */
	//	public void setLogger(Logger logger)
	//	{
	//		this.logger = logger;
	//	}

	/**
	 * Objective : Sleep and catch the exception.
	 * 
	 * @param seconds
	 *            the number of seconds to wait
	 * @throws InterruptedException 
	 */
	public void wait(int seconds) throws InterruptedException
	{
		Thread.sleep(seconds * 1000);
	}

	/**
	 * Objective : To wait for page to load
	 * @throws MalformedURLException 
	 */
	public void waitForPageToLoad() throws MalformedURLException
	{
		//		logger.info("Waiting for page to load");
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>()
				{
			public Boolean apply(WebDriver driver)
			{
				return ((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete");
			}
				};

//				Wait<WebDriver> wait = new WebDriverWait(WebDriverManager.getThreadLocalDriver(), WAIT_PAGE_LOAD);
//				WebDriverWait wait = new WebDriverWait(driver,WAIT_PAGE_LOAD);
				WebDriverWait wait = new WebDriverWait(getDriver("ie", "local"),120);
				wait.until(expectation);
				//				logger.info("Page load completed.");		

	}

	/**
	 * Objective : To wait for given element (By) to be present
	 * @param locator
	 */
	public void waitForElementToBePresent(By locator)
	{
		new WebDriverWait(WebDriverManager.getThreadLocalDriver(), WAIT_EXPLICIT)
		.until(ExpectedConditions.presenceOfElementLocated(locator));
	}

	/**
	 * Objective : To wait for element to be invisible
	 * @param e
	 */
	public void waitForElementToBeVisible(WebElement e)
	{
		new WebDriverWait(WebDriverManager.getThreadLocalDriver(), WAIT_EXPLICIT)
		.until(ExpectedConditions.visibilityOf(e));
	}

	/**
	 * Objective : To wait for element (By) to be invisible
	 * @param locator
	 */
	public void waitForElementToBeInvisible(By locator)
	{
		new WebDriverWait(WebDriverManager.getThreadLocalDriver(), WAIT_EXPLICIT)
		.until(ExpectedConditions.invisibilityOfElementLocated(locator));
	}

	/**
	 * Objective : To wait for button to be clickable
	 * 
	 * @param element
	 */
	public void waitForElementToBeClickable(WebElement element)
	{
		new WebDriverWait(WebDriverManager.getThreadLocalDriver(), WAIT_EXPLICIT)
		.until(ExpectedConditions.elementToBeClickable(element));
	}

	/**
	 * Objective : To check whether element is displayed or not
	 * 
	 * @param e
	 * @return
	 */
	public boolean isElementDisplayed(WebElement e)
	{
		return e.isDisplayed();
	}

	/**
	 * Objective : To scroll down to given vertical point
	 * 
	 * @param verPoint
	 *            the vertical point
	 * @throws InterruptedException 
	 */
	public void moveScrollBar(int verPoint) throws InterruptedException
	{
		((JavascriptExecutor) WebDriverManager.getThreadLocalDriver())
		.executeScript("window.scroll(0, " + verPoint + ");");
		this.wait(1);
	}

	/**
	 * Objective : To scroll down to given Web Element
	 * 
	 * @param e
	 * @return
	 * @throws InterruptedException 
	 */
	public WebElement scrollToElement(WebElement e) throws InterruptedException
	{
		Coordinates coordinate = ((Locatable) e).getCoordinates();
		coordinate.onPage();
		coordinate.inViewPort();
		moveScrollBar(e.getLocation().y);
		return e;
	}

	/**
	 * Objective: To clear the element
	 * 
	 * @param e
	 */
	public void clear(WebElement e)
	{
		e.clear();
	}

	/**
	 * Objective: To wait for Ajax request to complete
	 * @throws InterruptedException 
	 */
	public void waitForAjaxToLoad() throws InterruptedException 
	{
		//		logger.info("Checking active ajax calls by calling jquery.active");
		if (WebDriverManager.getThreadLocalDriver() instanceof JavascriptExecutor) 
		{
			JavascriptExecutor jsDriver = (JavascriptExecutor)WebDriverManager.getThreadLocalDriver();

			for (int i = 0; i< WAIT_PAGE_LOAD; i++) 
			{
				Object numberOfAjaxConnections = jsDriver.executeScript("return jQuery.active");

				if (numberOfAjaxConnections instanceof Long) 
				{
					Long number = (Long)numberOfAjaxConnections;

					//		           System.out.println("Number of active jquery ajax calls: " + number);
					if (number.longValue() == 0L)
						break;
				}
				wait(1);
			} 
		}
		else 
		{
			//			logger.info("Web driver: " + WebDriverManager.getThreadLocalDriver() + " cannot execute javascript");
		}
		//		logger.info("AJAX Wait finished");
	}

	/**
	 * Change Style Tag Run Time of an Element
	 * 
	 * @param WebElement
	 *            ID, TagName, NewValue WebElement for which style need to
	 *            change at runtime TagName for which the new value need to
	 *            change NewValue for changed value
	 */
	public void changeStyleAttrValue(String elementID, String TagName,
			String NewValue) {

		JavascriptExecutor js = (JavascriptExecutor) WebDriverManager.getThreadLocalDriver();

		String executeScriptText = "document.getElementById('" + elementID
				+ "').setAttribute('" + TagName + "', '" + NewValue + "')";
		js.executeScript(executeScriptText);
	}

	/**
	 * Change Style Tag Run Time of an Element
	 * 
	 * @param WebElement
	 *            ID, TagName, NewValue WebElement for which style need to
	 *            change at runtime TagName for which the new value need to
	 *            change NewValue for changed value
	 */
	public void changeStyleWithCssSelector(WebElement element) {

		JavascriptExecutor js = (JavascriptExecutor) WebDriverManager.getThreadLocalDriver();
		js.executeScript("arguments[0].setAttribute('style', arguments[1]);",
				element, "display:block;");
	}

	/**
	 * Change Style Tag Run Time of an Element
	 * 
	 * @param WebElement
	 *            ID, TagName, NewValue WebElement for which style need to
	 *            change at runtime TagName for which the new value need to
	 *            change NewValue for changed value
	 */
	public void changeStyleAttrValueByXpath(String elementID, String TagName,
			String NewValue) {

		JavascriptExecutor js = (JavascriptExecutor) WebDriverManager.getThreadLocalDriver();

		String executeScriptText = "document.getElementByXPath('" + elementID
				+ "').setAttribute('" + TagName + "', '" + NewValue + "')";
		js.executeScript(executeScriptText);
	}

	/**
	 * Objective : To wait for By to be invisible
	 * @author rajesham
	 * @param e
	 */
	public void waitForElementToBeVisible(By e)
	{
		new WebDriverWait(WebDriverManager.getThreadLocalDriver(), WAIT_EXPLICIT)
		.until(ExpectedConditions.visibilityOfElementLocated(e));
	}

	/**
	 * @author rajesham
	 * Objective : To check the check box
	 * @param e
	 */
	public boolean checkCheckBox(WebElement e) 
	{
		if(!e.isSelected())
		{
			click(e);
			return true;
		}
		else
			return false;
	}

	/**
	 * @author rajesham
	 * Objective : To uncheck the check box
	 * @param e
	 */
	public boolean uncheckCheckBox(WebElement e) 
	{
		if(e.isSelected())
		{
			click(e);
			return true;
		}
		else
			return false;
	}

	/**
	 * Objective : To verify if element is present 
	 * @author rajesham
	 */
	public boolean isElementPresent(By by) 
	{
		boolean present;
		try
		{
			WebDriverManager.getThreadLocalDriver().findElement(by);
			present = true;
		}catch (NoSuchElementException e)
		{
			present = false;
		}
		return present;
	}

	public String getText(List<WebElement> breadcrumb) {
		// TODO Auto-generated method stub
		String str="";

		for(WebElement element:breadcrumb)
			str=str+element.getText();
		return str ;
	}

	/**
	 * Objective : To scroll to given Web Element horizontally
	 * 
	 * @param e
	 * @return
	 * @throws InterruptedException 
	 */
	public WebElement scrollToElementHorizontally(WebElement e) throws InterruptedException
	{
		Coordinates coordinate = ((Locatable) e).getCoordinates();
		coordinate.onPage();
		coordinate.inViewPort();
		//	  moveScrollBar(e.getLocation().x);
		((JavascriptExecutor) WebDriverManager.getThreadLocalDriver())
		.executeScript("window.scroll(2000, 0);");
		this.wait(1);

		return e;
	}

	/**
	 * @author rajesham
	 * @return This is for switching driver from one browser window to another. 
	 * @throws MalformedURLException 
	 */ 
	public void SwitchWindow() throws MalformedURLException{

		String winHandleBefore = WebDriverManager.getThreadLocalDriver().getWindowHandle();
		//Switch to new window opened
		for(String winHandle : WebDriverManager.getThreadLocalDriver().getWindowHandles()){
			WebDriverManager.getThreadLocalDriver().switchTo().window(winHandle);
		}
		waitForPageToLoad();

		//For closing the new window, if that window no more required
		WebDriverManager.getThreadLocalDriver().close();

		//Switch back to original browser (first window)

		WebDriverManager.getThreadLocalDriver().switchTo().window(winHandleBefore);


	}
	
	
}
