package functionLibraries;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;

public class WebDriverManager 
{
	private static ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>();

	public static WebDriver getThreadLocalDriver() 
	{
		return webDriver.get();
	}

	public static void setThreadLocalWebDriver(WebDriver driver) 
	{
		driver.manage().timeouts().implicitlyWait(Action.WAIT_IMPLICIT, TimeUnit.MILLISECONDS);
		maximizeWindow(driver);
		webDriver.set(driver);
	}

	public static void maximizeWindow(WebDriver driver) 
	{
		driver.manage().window().setPosition(new Point(0, 0));
		java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		Dimension dim = new Dimension((int) screenSize.getWidth(),(int) screenSize.getHeight());
		driver.manage().window().setSize(dim);
	}
}
