package functionLibraries;

import java.io.File;
import java.util.Properties;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;


import pageObject.commonPO;

public class CommonLibrary  extends commonPO{

	protected Action action =null;
	protected WebDriver driver;
	
	protected CommonUtil commonUtil;
	protected String mainWindowID;
	
	
	
//	String newpropertyname= propertiesProp.getProperty("newpropertyname")+commonUtil.getTimeStamp();

	public static final File INPUT_PROPERTIES_ROOT_DIRECTORY = new File("test-input");
	public static final File INPUT_PROPERTIES_BASE_DIRECTORY = new File(INPUT_PROPERTIES_ROOT_DIRECTORY, "uat");
	public static Properties consoleProps = CommonUtil.openPropertyFile(new File(INPUT_PROPERTIES_BASE_DIRECTORY, "consoleInput.properties"));
	public static Properties createlotProps= CommonUtil.openPropertyFile(new File(INPUT_PROPERTIES_BASE_DIRECTORY, "createlot.properties"));
	public static Properties createcontractcardProps= CommonUtil.openPropertyFile(new File(INPUT_PROPERTIES_BASE_DIRECTORY, "createcontractcard.properties"));
	
	
	public static final File TEST_DATA = new File(INPUT_PROPERTIES_ROOT_DIRECTORY, "Media");
//	public static final File IMAGES= new File(TEST_DATA, "Images");
//	public static Properties addimage = CommonUtil.openPropertyFile(new File(TEST_DATA,"Images"));
	
	
	
	public CommonLibrary(WebDriver driver)
	{
		super(driver);
		this.driver = driver;
		action = new Action();
		commonUtil = new CommonUtil();
		
	}

	/**
	 * Objective : To login in to the application
	 * 
	 * @param userName
	 * @param password
	 */
	public void login(String URL, String userName, String password) throws Exception 
	{
//		driver.get(URL);
//		System.out.println(URL);
//		action.waitForPageToLoad();
//		action.enterValue(Username, userName, true);
//		action.enterValue(Password, password);
//		action.click(btnLogin);
//		action.waitForPageToLoad();
//		action.waitForElementToBeVisible(loggedInUser);
		//		Assert.assertEquals(action.getText(loggedInUser), "Hi, " + userName);
	}

	public String getConsoleInputs(String key)
	{
		return consoleProps.getProperty(key);
	}

	
	
	

	
	
	
	
	public void Profile(){
		
		
	
	}
	
	public void switchToWindow() 
	{
		this.mainWindowID = driver.getWindowHandle();
		driver.switchTo().window(mainWindowID);
	}
	
	public void switchDefaultWindow()
	{
		String winHandleBefore = WebDriverManager.getThreadLocalDriver().getWindowHandle();
		WebDriverManager.getThreadLocalDriver().switchTo().window(winHandleBefore);
	}
	
	
	/**
	 * Objective: Switch to frame from the window 
	 * @param FrameID : provide the frame ID from PO class
	 * @author rajesham
	 */
	public void switchToFrame(WebElement frameID)
	{
		driver.switchTo().frame(frameID);
	}
	
	
	
	
}





