package functionLibraries;



import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
//import org.apache.poi.hssf.usermodel.HSSFSheet;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.Alert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.testng.Assert;

public class CommonUtil
{
//	private static final Logger logger = new Logger(CommonUtil.class);

	/**
	 * Objective : Method to get invoking method's name from stack trace
	 * @param n
	 * @return method name
	 */

	public String getMethod(int n)
	{
		StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
		StackTraceElement e = stacktrace[n];
		return e.getMethodName();
	}

	/**
	 * Objective : Method to assert if null
	 * 
	 * @param condition
	 * @param msg
	 */
	public void assertNull(Object condition, String msg)
	{
		if (condition != null)
			takeScreenShot(getMethod(2));
		Assert.assertNull(condition, msg);
	}

	/**
	 * Objective : Method to assert if not null
	 * 
	 * @param condition
	 * @param msg
	 */
	public void assertNotNull(Object condition, String msg)
	{
		if (condition == null)
			takeScreenShot(getMethod(2));
		Assert.assertNotNull(condition, msg);
	}

	/**
	 * Objective : Method to take screenshot of current driver instance
	 * 
	 * @param MethodName
	 * Modification History - 1) Updated function to take screen shots of failed test methods 
	 *                          which are executing on remote machine (Node) 
	 */
	public void takeScreenShot(String MethodName)
	{			
		try	{
			WebDriver augmentedDriver = new Augmenter().augment(WebDriverManager.getThreadLocalDriver());
			File source = ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE);

			String path = System.getProperty("user.dir") + "\\test-output\\screenshots\\"+ MethodName + "_"+getTimeStamp() + ".png";
			FileUtils.copyFile(source, new File(path));
		}
		catch (Exception e)
		{
			e.printStackTrace();
//			logger.info("Failed to take screen shot of remote browser");
		}                          
	}

	/**
	 * Objective : Method to open specified property file
	 * @param file
	 * @return 
	 */
	public static Properties openPropertyFile(File file)
	{
		Properties propertyFile = new Properties();
		try
		{
			FileInputStream fis = new FileInputStream(file);
			propertyFile.load(fis);
			fis.close();
		} catch (IOException e)
		{
			e.printStackTrace();
			propertyFile = null;
		}
		return propertyFile;
	}

	/**
	 * Objective : To get current time stamp in _yyMMdd_HHmmss format
	 * @return
	 */
	public String getTimeStamp()
	{
		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar
				.getInstance().getTime());
		return timeStamp;
	}

	/**
	 * Objective : To capture failure when any Exception occurs
	 * @param e
	 */
	public void captureFailure(Exception e)
	{
		e.printStackTrace();
		String MethodName = getMethod(3);
		takeScreenShot(MethodName);
		Assert.fail("TEST FAILURE :: Exception Failure caused by " + MethodName,e);

	}

	/**
	 * Objective : To capture failure when any Assertion fails
	 * @param e
	 */
	public void captureFailure(AssertionError e)
	{
		System.out.println("@@@@@@@@@@@@$$$$$$$$$$$$$$$$$$$$465646464646464");
		e.printStackTrace();
		String MethodName = getMethod(3);
//		logger.info("**************** Test Failure (Assertion) :: "+ MethodName + " has FAILED **************");
		System.out.println("@@@@@@@@@@@@$$$$$$$$$$$$$$$$$$$$");
		takeScreenShot(MethodName);
		Assert.fail("TEST FAILURE :: Assertion Failure caused by " + MethodName,e);
	}


	/**
	 * Objective : This function is to close alert and fetch its message contents
	 * @param acceptNextAlert
	 * @return String, alert text content
	 * @throws InterruptedException
	 */
	public String acceptAlertAndGetMessage(boolean acceptNextAlert) throws InterruptedException 
	{
		Alert alert = WebDriverManager.getThreadLocalDriver().switchTo().alert();
		String alertText = alert.getText();
		if (acceptNextAlert) {
			alert.accept();
		} else {
			alert.dismiss();
		}
		return alertText;
	}
	
	/**
	 * Objective : Method to get next date from the current date
	 * @author rajesham
	 * Last Modified :
	 * Modification History :
	 */
	public String getNextDate() 
	{
//		logger.info("---------- Method getNextDate Started ----------");
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 1);
		String nextDate = sdf.format(cal.getTime());
//		logger.info("---------- Method getNextDate Ended ----------");
		return nextDate;
	}
	
	
	public String getTodayDate() {
//		logger.info("-----------Method todays date started");
		String startDate = new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance()
				.getTime());
//		logger.info("-----------Method todays date Ended");
		return startDate;
	}
	
	public String getNextMonthDate() {
//		logger.info("--------- Method next month date started");
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 30);
		String nextDate = sdf.format(cal.getTime());
//		logger.info("--------- Method next month date ended");
		return nextDate;
	}


	/**
	 * Objective - To write in an excel file on the given (row,column) cell 
	 * @author rajesham 
	 * @param FilePath
	 * @param Row
	 * @param Column
	 * @param Value
	 * @throws IOException
	 */
	
//	public void writeValueInExcel(String FilePath, int Row, int Column, String Value) throws IOException
//    {
//    	FileInputStream file = new FileInputStream(new File(FilePath));
//	    HSSFWorkbook workbook = new HSSFWorkbook(file);
//	    HSSFSheet sheet = workbook.getSheetAt(0);
//	    
//	    Row = Row-1;
//	    Column= Column-1;
//    	
//	    Row row = sheet.getRow(Row);	
//	    if(row == null)
//    	    	row = sheet.createRow(Row);
//    	
//    	Cell cell = row.getCell(Column);
//	    if(cell == null)
//	    	cell = row.createCell(Column);
//	    
//	    cell.setCellValue(Value);
//	    file.close();
// 	   	
//	    FileOutputStream outFile =new FileOutputStream(new File(FilePath));
// 	   	workbook.write(outFile);
// 	   	outFile.close();    	  
//    }
//	
//	public void ReadValueFExcel(String FilePath, int Row, int Column, String Value) throws IOException
//    {
//    	FileInputStream file = new FileInputStream(new File(FilePath));
//	    HSSFWorkbook workbook = new HSSFWorkbook(file);
//	    HSSFSheet sheet = workbook.getSheetAt(0);
//	    
//	    Row = Row-1;
//	    Column= Column-1;
//    	
//	    Row row = sheet.getRow(Row);	
//	    if(row == null)
//    	    	row = sheet.createRow(Row);
//    	
//    	Cell cell = row.getCell(Column);
//	    if(cell == null)
//	    	cell = row.createCell(Column);
//	    
//	    cell.setCellValue(Value);
//	    file.close();
// 	   	
//	    FileOutputStream outFile =new FileOutputStream(new File(FilePath));
// 	   	workbook.write(outFile);
// 	   	outFile.close();    	  
//    }

}
